![Gatling logo](img/logo.png)

Simple load tests example using the Gatling ([https://gatling.io]()) and gatling maven plugin
    
Execute test scenario:

    ./mvnw clean gatling:test
      
The report path is: [.../target/gatling/carssearchsimulation-${build-version}/index.html]()

![Gatling report](img/report_part1.png)

![Gatling report](img/report_part2.png)
![Gatling report](img/report_part3.png)
![Gatling report](img/report_part4.png)