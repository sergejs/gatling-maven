package com.ss

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class CarsSearchSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("https://ss.com/lv/")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36")

  object SearchSteps {
    val requestAllCars = exec(http("Request all cars")
      .get("transport/cars/")
      .check(status.is(200)))

    def requestByBrand(brand:String) = exec(http(s"Request $brand cars")
      .get(s"transport/cars/$brand/")
      .check(status.is(200)))

    def requestByBrandAndModel(brand:String, model: String) = exec(http(s"Request $brand $model cars")
      .get(s"transport/cars/$brand/$model")
      .check(status.is(200)))
  }

  val scn = scenario("Scenario Cars Search")
    .exec(
      SearchSteps.requestAllCars,
      SearchSteps.requestByBrand("volvo"),
      SearchSteps.requestByBrandAndModel("volvo", "s80"),
      SearchSteps.requestByBrandAndModel("volvo", "s90"),
      SearchSteps.requestByBrandAndModel("volvo", "v50"),
      SearchSteps.requestByBrandAndModel("volvo", "v70"),
      SearchSteps.requestByBrandAndModel("volvo", "xc70"),
      SearchSteps.requestByBrandAndModel("volvo", "xc90"),
      SearchSteps.requestByBrand("shkoda"),
      SearchSteps.requestByBrandAndModel("shkoda", "fabia"),
      SearchSteps.requestByBrandAndModel("shkoda", "fabia"),
      SearchSteps.requestByBrandAndModel("shkoda", "octavia"),
      SearchSteps.requestByBrandAndModel("shkoda", "superb"),
      SearchSteps.requestByBrandAndModel("shkoda", "karoq"),
      SearchSteps.requestByBrandAndModel("shkoda", "kodiaq"),
    )

  setUp(scn.inject(atOnceUsers(50)).protocols(httpProtocol))
}
